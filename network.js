function network() {
    var width = 1280,
        height = 960;
    
    var color = d3.scale.category20(),
        linksColor = d3.scale.category10();
    
    function draw(graph) {
	var force = d3.layout.force()
	    .charge(-120)
	    .linkDistance(60)
	    .size([width, height]);

	var svg = d3.select(".view").append("svg")
	    .attr("width", width)
	    .attr("height", height);
	
	var linkroles = d3.map(graph).keys();

	function update(new_graph) {
	    d3.selectAll(".node").remove();
	    d3.selectAll(".link").remove();
	    force
		.nodes(new_graph.nodes)
		.links(new_graph.links)
		.start();
	    
	    var new_link = svg.selectAll(".link").data(new_graph.links)
		.enter().append("line")
		.attr("class", "link")
		.style("stroke-width", 1.5)
		.style("stroke", function(d) { return linksColor(linkroles.indexOf(d.linkrole)); });
	    
	    var new_node = svg.selectAll(".node").data(new_graph.nodes)
		.enter().append("circle")
		.attr("class", "node")
		.attr("r", 5)
		.style("fill", function(d) { return color(0); })
		.style("cursor", "pointer")
		.on("mouseover", function(d) {
		    var this_e = d3.select(this);
		    var x = this_e.attr("cx"),
		    y = this_e.attr("cy");
		    
		    this_e.classed("selected", true)
			.transition().duration(250)
			.attr("r", 12);
		    
		    svg.append("text")
			.attr("x", x + 10)
			.attr("y", y - 10)
			.classed("tooltip", true)
			.style("fill", "#555")
			.style("font", "14px Georgia")
			.text(d.name.split("#")[1]);
		})
		.on("mouseout", function() {
		    d3.selectAll(".tooltip").remove();
		    d3.select(this).classed("selected", false)
			.attr("r", 5);
		})
		.call(force.drag);
	    
	    
	    force.on("tick", function() {
		new_link.attr("x1", function(d) { return d.source.x; })
		    .attr("y1", function(d) { return d.source.y; })
		    .attr("x2", function(d) { return d.target.x; })
		    .attr("y2", function(d) { return d.target.y; });
		
		new_node.attr("cx", function(d) { return d.x; })
		    .attr("cy", function(d) { return d.y; });
	    });

	}
  
      
  
	var linkroleTexts = svg.selectAll(".linkroleText")
	    .data(linkroles)
	    .enter()
	    .append("text")
	    .text(function(d) { return d; })
	    .attr("x", 10)
	    .attr("y", function(d, i) { return 20 + i * 20; })
	    .attr("fill", function(d,i) { return linksColor(i); })
	    .style("font-size", 15)
	    .style("cursor", "pointer")
	    .on("click", function(d) {
		console.log(d);
		update(graph[d]);
	    });
	console.log("start");
	update(graph[linkroles[0]]);

    }


    draw.width = function(value) {
	if(!arguments.length) return width;
	width = value;
	return draw;
    };

    draw.height = function(value) {
	if(!arguments.length) return height;
	height = value;
	return draw;
    };

    return draw;
}
