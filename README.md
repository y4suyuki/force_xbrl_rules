This simple force-directed graph shows character co-occurence in *Les Misérables*. A physical simulation of charged particles and springs places related characters in closer proximity, while unrelated characters are farther apart. Layout algorithm inspired by [Tim Dwyer](http://www.csse.monash.edu.au/~tdwyer/) and [Thomas Jakobsen](http://web.archive.org/web/20080410171619/http://www.teknikus.dk/tj/gdc2001.htm). Data based on character coappearence in Victor Hugo's *Les Misérables*, compiled by [Donald Knuth](http://www-cs-faculty.stanford.edu/~uno/sgb.html).

Compare this display to a force layout with [curved links](/mbostock/4600693), a force layout with [fisheye distortion](http://bost.ocks.org/mike/fisheye/) and a [matrix diagram](http://bost.ocks.org/mike/miserables/).

--------------------------------------------------------------------------------
forked from Mike Bostok's simple force-directed graph gist.
Showing the relationship of each element of a Japanese campany's financial report.
These relationships are derived directory from XBRL data.

to see the example, python's simplehttpserver is easy way to start web server.
```sh
% python -m SimpleHTTPServer 9000
```

