#!/bin/bash
target=~/Dropbox/Public/d3portfolio/force
cp *.js $target
echo "Copied all javascript files."
cp *.html $target
echo "Copied all html files."
cp *.css $target
echo "Copied all css files."
cp ./data/* $target/data/
echo "Copied all data files in the data directory."
